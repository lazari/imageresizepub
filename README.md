# Képek csoportos átméretezése

Az alkalmazás képek csoportos átméretezését és vízjellel való ellátást teszi lehetővé.
Bővebben: [wiki](https://gitlab.com/lazari/imageresizepub/wikis/home).